const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
const keys = require('../config/keys');

const AdminUser = mongoose.model('adminusers');

passport.serializeUser((user, done) => {
  done(null, user.id); // this id is from mongodb _id recored
});

passport.deserializeUser(async (id, done) => {
  const user = await AdminUser.findById(id);
  done(null, user);
});

passport.use(
  new GoogleStrategy(
    {
      clientID: keys.googleClientID,
      clientSecret: keys.googleClientSecret,
      callbackURL: '/auth/google/callback'
    },
    async (accessToken, refreshToken, profile, done) => {
      let userExists = await AdminUser.findOne({ googleId: profile.id });
      if (userExists) {
        // user already exists log him in
        console.log('successful login', userExists);
        done(null, userExists);
      } else {
        // remove the user's email that set by the super admin and then creat new one
        await AdminUser.findOne({
          email: profile.emails[0].value
        })
          .remove()
          .exec();
        const user = await new AdminUser({
          googleId: profile.id,
          email: profile.emails[0].value,
          fullName: profile.displayName,
          imageURL: profile.photos[0].value
        }).save();
        done(null, user);
      }
      // console.log(profile.emails[0].value);
    }
  )
);
