const mongoose = require('mongoose');
const Parent = mongoose.model('parents');
const Expo = require('expo-server-sdk');

// Create a new Expo SDK client
let expo = new Expo(); 
let message = {
    to: "ExponentPushToken[MCi87NHiHkdILZNmaFKiEc]",
    sound: 'default',
    body: 'This is a test notification',
    data: { withSome: 'data' }
};
( async () => {
    try {
        let receipts = await expo.sendPushNotificationsAsync(message);
        console.log('reponse of sending push notification', receipts);
    } catch (err) {
        console.log('erro of sending PN', err);
    }    
})();
