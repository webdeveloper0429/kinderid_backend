const _ = require('lodash');
const mongoose = require('mongoose');
const path = require('path');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const stripe = require('stripe')('sk_test_05VBjhFZ65fonp5ceecWwct7');
const moment = require('moment');
const verifyToken = require('../../middleware/verifyToken');
const axios = require('axios');

const Parent = mongoose.model('Parent');
const Subscription = mongoose.model('Subscription');

module.exports = app => {
  app.post('/desktop/updateprofile', verifyToken, async (req, res) => {
    const { _id } = req.user.user;
    const { parentName, mobile, email } = req.body;
    try {
        await Parent.findOneAndUpdate(
          {_id},
          {$set: {
            parentName,
            mobile,
            email
          }}
        );
        res.status(200).send("Profile Updated");
    } catch (err) {
        console.log('error is: ', err.message);
        res.status(400).send("Error occured. Profile was not updated.");
    }
  });
  app.post('/desktop/updateshipping', verifyToken, async (req, res) => {
    const { _id } = req.user.user;
    const { country, address, postalCode, city } = req.body;
    try {
        await Parent.findOneAndUpdate(
          {_id},
          {$set: {
            "country.name": country,
            address,
            postalCode,
            city
          }}
        );
        res.status(200).send("Profile Updated");
    } catch (err) {
        console.log('error is: ', err.message);
        res.status(400).send("Error occured. Profile was not updated.");
    }
  });
  app.post('/desktop/changepassword', verifyToken, async (req, res) => {
    const { _id } = req.user.user;
    console.log('hi');

    const { password, npassword } = req.body;
    if (!password && !npassword) {
      console.log('no password');

      return res.status(400).send("Unauthorized attemp.");
    }
    try {
      const user = await Parent.findById({_id});
      console.log('user found');

      user.comparePassword(password, async (err, isMatched) => {
        if (err) res.status(401).send("Something went wrong! Please re-try later");
        if (!isMatched) {
          console.log('wronge password');

          return res.status(401).send("Wrong Password!");
        }
        user.password = npassword;
        user.save(saveError => {
          if (saveError) {
            console.log('save error');

            res.status(401).send("Something went wrong! Error in changing the password");
          }
          console.log('success');

          return res.status(200).send("Profile Updated");
        });
      });
    } catch (err) {
      console.log('error is: ', err.message);
      res.status(400).send("Error occured. Profile was not updated.");
    }
  });
  // mokcing
  app.post('/desktop/changeuserstatus', verifyToken, async (req, res) => {
    const { _id } = req.user.user;
    try {
      await Parent.findOneAndUpdate(
        {_id},
        {status: "NOCHILD"}
      );
      return res.status(200).send("Profile Updated");
    } catch (err) {
      console.log('error is: ', err.message);
      res.status(400).send("Error occured. Profile was not updated.");
    }
  });

  // end of router
};
