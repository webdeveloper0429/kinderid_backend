const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const verifyToken = require('../../middleware/verifyToken');
const keys = require('../../config/keys');

const Parent = mongoose.model('Parent');
const RegisterParent = mongoose.model('registerparents');


module.exports = app => {
  app.post("/auth/desktop/checkemail", async (req, res) => {
    const email = (req.body.email).toLowerCase();
    try {
      const user = await Parent.findOne({email}).exec();
      if (user) {
        if (user.active) {
          return res.status(200).json({email, message: "authorized_email"});
        }
        return res.status(401).send("User is deactivated");
      }
    } catch (err) {
      return console.log('error is: ', err.message);
    }
    //user is not registered
    return res.status(200).json({ email, message: "new_user" });
//     RegisterParent.findOne({email}, async (err, registerdEmail) => {
//     if (registerdEmail) {
//       return res.status(200).json({ registerdEmail: registerdEmail.email, message: "new_user"});
//     } else {
//       await new RegisterParent({
//         email,
//         created_at: Date.now()
//       }).save();
//       return res.status(200).json({ registerdEmail: email, message: "new_user" })
//     }
//   });
  });

  app.post("/auth/desktop/registeruser", async (req, res) => {
    console.log('booooooooody', req.body.user);
    const { email, fullName, mobile, address, post, city, password } = req.body.user;
    new Parent({
        email,
        parentName: fullName,
        mobile,
        password,
        address,
        postalCode: post,
        city,
        country: { // TODO fix the error when register user without providing couontry.name
          name: ''
        }
      }).save((err, user) => {
        if (err) {
          return console.log('save error', err);
        }
        console.log('registered user', user);

        const token = jwt.sign({user}, keys.jwtSecret, {expiresIn: '1h'});
        console.log('token', token);

        return res.status(200).json({ token });
      });
  });

//   app.post('/auth/mobile/validate_user', async (req, res) => {
//     console.log('requesting: ', req.body.user);

//     const {user, verificationCode} = req.body;
//     const {email} = user;
//     if (!email) {
//       return res.status(401).send("Unauthorized")
//     }

//     RegisterParent.findOne({email, verificationCode}, (err, result) => {
//       if(true) return res.status(200).json(user); // only for testing
//       if(result) return res.status(200).json(user)
//       return res.status(401).json({user, message: "Wrong Verification Code"});
//     })
//   });

//   app.post('/auth/mobile/authorize_user', async (req, res) => {
//     const {email, password } = req.body
//     console.log('email', email);
//     console.log('password', password);
//     if (!email || !password){
//       return res.status(400).send("Bad Request!");
//     }


//     Parent.findOne({email}, (err, user) => {
//       console.log(user);
//       if (!user || err) return res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
//       user.comparePassword(password, (err, isMatched) => {
//         if (err) res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
//         if (isMatched) {
//           if (!user.active) {
//             return res.status(401).json({ authorized: false, message: "Your user has been temporarily deactivated. Please contact the customer service for more information." });
//           }
//           user.login_attemps = 0;
//           user.save();
//           user = user.toObject();
//           delete user.password;
//           var token = jwt.sign({user}, keys.jwtSecret, {expiresIn: '1h'});
//           return res.status(200).json({ authorized: true, token });
//         }
//         let message = "Password does not match!"
//         user.login_attemps += 1;
//         if(user.login_attemps >= 5) {
//           user.active = false;
//           message = "User has been deactivated!"
//         }
//         console.log('user', user);
//         user.save()
//         return res.status(401).json({ authorized: false, message});

//       })
//     });
//   });

//   app.post('/auth/verifyToken', verifyToken, (req, res) => {
//     return res.status(200).json({verifiedToken: true});
//   });

//   app.post('/auth/mobile/getuserfromtoken', verifyToken, (req, res) => {
//     res.status(200).json({token: req.user});
//   });

//   app.post('/jwt/test_register', async (req, res) => {
//     const respond = await new Parent({
//       email: req.body.email,
//       password: req.body.password,
//       wristband: req.body.wristband
//     }).save();
//     res.status(200).send("Done!");

//   });

//   app.get('/test/belal', async (req, res) => {
//     console.log('hi');

//     const _id = "5aa5a08a7bc16b16d495fa5d";
//     const user = await Parent.findOneAndUpdate(
//       {_id, "children._id": "5aa85deebbe068336c6ed69d"},
//       {
//           "children.$.active": true,

//       }
//     );
//     res.json({user});
//   });
};
