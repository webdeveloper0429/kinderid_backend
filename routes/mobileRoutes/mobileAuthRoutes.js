const mongoose = require('mongoose');
const sgMail = require('@sendgrid/mail');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const verifyToken = require('../../middleware/verifyToken');
const keys = require('../../config/keys');
const moment = require('moment');

const Parent = mongoose.model('Parent');
const RegisterParent = mongoose.model('registerparents');

sgMail.setApiKey(keys.sendGridKey);


module.exports = app => {
  app.post("/auth/mobile/email_login", async (req, res) => {
    let { email } = req.body;
    if (!email) {
      return res.status(400).send("No Email Provided");
    }
    email = (email).toLowerCase();

    const sendEmail = user => {
      // return res.status(200).json({ email: user.email, status: user.status });
      const msg = {
        to: user.email,
        from: 'bel.nabhani@gmail.com',
        subject: 'Register Verification Code',
        html: `<html>
                <body>
                  <h2>Kinder-id</h2>
                  <h4>You are successfully registered</h4>
                  <h4>You verification code is: <strong>${user.verificationCode}</strong></h4>
                </body>
              </html>`
      };
      sgMail.send(msg, (err) => {
        if (err) {
          console.log('error', err);
          return res.status(406).send("Error Sending Email");
        }
        console.log('email sent');
        return res.status(200).json({ email: user.email, status: user.status });
      });
    };
    try {
      const user = await Parent.findOne({email}).exec();
      if (user) {
        if (!user.active) {
          return res.status(401).send("User is deactivated");
        }
        if (user.status === "INVITED" || user.status === "NEW") {
          return sendEmail(user);
        }
        return res.status(200).json({email, status: user.status});
      }
    } catch (err) {
      console.log('error is: ', err.message);
    }
    const verificationCode = Math.floor(Math.random() * 900000) + 100000;
    await new Parent({
      email,
      verificationCode,
    }).save((err, newUser) => {
      if (err) {
        console.log('error saving user', err);
      }
      // return res.status(200).json({ email: newUser.email, status: newUser.status }); // only for testing: dont send emails
      sendEmail(newUser);
    });
  });

  app.post('/auth/mobile/validate_user', async (req, res) => {
    const {email, verificationCode} = req.body;
    console.log('body=====', req.body);

    if (!email) {
      return res.status(401).send("Unauthorized");
    }
    Parent.findOne({email, verificationCode}, (err, result) => {
      console.log('result', result);

      // if (true) return res.status(200).json(user); // only for testing
      if (err) {
        return res.status(401).send("User not found");
      }
      if (result) return res.status(200).send("OK");
      return res.status(401).send("Wrong Verification Code");
    });
  });

  app.post('/auth/mobile/authorize_user', async (req, res) => {
    const {email, password } = req.body;
    console.log('email', email);
    console.log('password', password);
    if (!email || !password) {
      return res.status(400).send("Bad Request!");
    }
    Parent.findOne({email}, (err, user) => {
      if (!user || err) return res.status(401).send("Something went wrong! Please re-try later");
      user.comparePassword(password, (passError, isMatched) => {
        if (passError) res.status(401).send("Something went wrong! Please re-try later");
        if (isMatched) {
          if (!user.active) {
            return res.status(401).send("Your user has been temporarily deactivated. Please contact the customer service for more information.");
          }
          user.login_attemps = 0;
          user.save();
          // user = user.toObject();
          // delete user.password;
          const token = jwt.sign({user}, keys.jwtSecret, {expiresIn: '24h'}); // only for testing
          return res.status(200).json({ token });
        }
        let message = "Password does not match!";
        user.login_attemps += 1;
        if (user.login_attemps >= 5) {
          user.active = false;
          message = "User has been deactivated!";
        }
        user.save();
        console.log('wrong password');

        return res.status(401).send(message);
      });
    });
  });
  app.post('/auth/mobile/updatenewuserprofile', async (req, res) => {
    const {
      parentName,
      mobile,
      email,
      address,
      country,
      postalCode,
      status,
      password
    } = req.body;
    if (!email || !password || !status) {
      return res.status(400).send("email and password can't be empty");
    }
    const user = await Parent.findOne({email});
    if (user.status === "USER") {
      return res.status(401).send("Unauthorized attemp!");
    }
    user.parentName = parentName;
    user.mobile = mobile;
    user.address = address;
    user.country = country;
    user.postalCode = postalCode;
    user.password = password;
    if (user.status === "INVITED") {
      user.status = "GAURDIAN";
      user.save();
      const token = jwt.sign({user}, keys.jwtSecret, {expiresIn: '24h'}); // only for testing
      return res.status(200).json({ token });
    }
    user.save((err) => {
      if (err) {
        console.log('error update profile', err);
        return res.status(400).send("Error. User was not saved!");
      }
      return res.status(200).send("OK");
    });
  });

  app.post('/auth/verifyToken', verifyToken, (req, res) => res.status(200).json({verifiedToken: true}));

  app.post('/auth/mobile/getuserfromtoken', verifyToken, (req, res) => {
    res.status(200).json({token: req.user});
  });

  app.post('/jwt/test_register', async (req, res) => {
    const respond = await new Parent({
      email: req.body.email,
      password: req.body.password,
      wristband: req.body.wristband
    }).save();
    res.status(200).send("Done!");
  });

  app.get('/test/belal', async (req, res) => {
    console.log('hi');
    const user = await Parent.findOne({_id: "5ae6c4eb76c48f6c0a264b74", "children._id": "5ae6f1c07e992e01f3c01e41"});
    const child = user.children.id("5ae6f1c07e992e01f3c01e41");
    const end = moment(child.subscription_end).add('year', 1);
    res.status(200).send({user: user.children.id("5ae6f1c07e992e01f3c01e41"), end});
  });
};
