const _ = require('lodash');
const mongoose = require('mongoose');
const path = require('path');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const multer = require('multer');
const stripe = require('stripe')('sk_test_05VBjhFZ65fonp5ceecWwct7');
const moment = require('moment');
const verifyToken = require('../../middleware/verifyToken');
const axios = require('axios');
const sgMail = require('@sendgrid/mail');

const storage = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, path.resolve(__dirname, './temp/'));
    },
    filename(req, file, cb) {
        cb(null, `${file.originalname}-${Date.now()}.jpg`);
      }
});
const upload = multer({ storage }).single('avatar');

const Parent = mongoose.model('Parent');
const Subscription = mongoose.model('Subscription');
const RegisterParent = mongoose.model('registerparents');

module.exports = app => {
    app.post('/mobile/getuserdata', verifyToken, async (req, res) => {
        const { _id } = req.user.user;
        const invitingParents = [];
        try {
            const user = await Parent.findById({_id});
            if (user) {
                if (!user.active) {
                    return res.status(401).send("User is deactivated");
                }
                if (user.invitingParentIds.length > 0) {
                const tempParent = await Parent.find({_id: {$in: user.invitingParentIds} });
                invitingParents.push(_.map(tempParent, ({_id, parentName}) => ({
                        invitingParentId: _id,
                        invitingParetName: parentName
                    })));
                }
                return res.status(200).json({user, invitingParents});
              }
        } catch (err) {
            console.log('error is: ', err.message);
        }
    });

    app.post('/mobile/addchildwithcard', verifyToken, (req, res) => {
        upload(req, res, async (err) => {
            if (err) {
                console.log('multer error', err);
                return res.status(419).json({message: 'Checksum failed'});
            }
            const { _id } = req.user.user;
            const {
                childrenName,
                wristband,
               } = req.body;

            if (!wristband) {
                return res.status(400).send("No card number provided!");
            }
            const parent = await Parent.findById({_id});
            try {
                const stripeResponse = await stripe.charges.create({
                    amount: 77600,
                    currency: 'nok',
                    customer: parent.card.id,
                    description: `Subscription for ${parent.parentName}`
                });
                const now = new Date();
                const subscription_end = now.setFullYear(now.getFullYear() + 1);
                const order = await new Subscription({
                    parentsID: _id,
                    parentName: parent.parentName,
                    childName: childrenName,
                    wristband,
                    subscription_end,
                    stripeResponse
                }).save();
                const user = await Parent.findById({_id});
                if (user) {
                    if (!user.active) {
                        return res.status(401).send("User is deactivated");
                    }
                    const newChild = {
                        childrenName,
                        wristband,
                        image: req.file.filename || "No Image Provided",
                        orderId: order._id,
                        subscription_end
                    };
                    user.children.push(newChild);
                    user.status = "USER";
                    user.save((error, user) => {
                        if (error) {
                            console.log('error saving ', error);
                            return res.status(400).json({code: error.code, message: "An error occured while saving data into the database!" });
                        }
                        const child = _.find(user.children, {wristband: newChild.wristband});
                        order.childID = child._id;
                        order.save();
                        return res.status(200).send({orderNo: order.orderNo});
                    });
                }
            } catch (err) {
                console.log('error is: ', err.message);
            }
        });
    });
    app.post('/payment/renewsubscription', verifyToken, async (req, res) => {
            const { _id } = req.user.user;
            const {
                type,
                childId,
               } = req.body;
               console.log('body', req.body);

            if (!childId) {
                return res.status(400).send("Bad request!");
            }
            const parent = await Parent.findById({_id});
            if (!parent.active) {
                return res.status(401).send("User is deactivated");
            }
            try {
                const stripeResponse = await stripe.charges.create({
                    amount: 77600,
                    currency: 'nok',
                    customer: parent.card.id,
                    description: `Subscription for ${parent.parentName}`
                });
                const now = new Date();
                let years = 1;
                if (type === "threeYear") {
                    years = 3;
                }
                const subscription_end = now.setFullYear(now.getFullYear() + years);
                const order = await new Subscription({
                    parentsID: _id,
                    parentName: parent.parentName,
                    childName: parent.childrenName,
                    wristband: parent.wristband,
                    subscription_end,
                    stripeResponse
                }).save();

                const child = parent.children.id(childId);
                child.subscription_end = moment(child.subscription_end).add(years, 'year');
                parent.save((error, user) => {
                    if (error) {
                        console.log('error saving ', error);
                        return res.status(400).json({code: error.code, message: "An error occured while saving data into the database!" });
                    }
                    order.childID = child._id;
                    order.save();
                    return res.status(200).send({last4: parent.card.last4, brand: parent.card.brand, orderNo: order.orderNo});
                });
            } catch (err) {
                console.log('error is: ', err.message);
            }
    });
    app.post('/payment/renewsubscriptionwithcard', verifyToken, async (req, res) => {
            const { _id } = req.user.user;
            const {
                stripeToken,
                type,
                childId,
               } = req.body;
               console.log('body', req.body);

            if (!childId) {
                return res.status(400).send("Bad request!");
            }
            const parent = await Parent.findById({_id});
            if (!parent.active) {
                return res.status(401).send("User is deactivated");
            }
            try {
                const stripeResponse = await stripe.charges.create({
                    amount: 77600,
                    currency: 'nok',
                    source: stripeToken,
                    description: `Subscription for ${parent.parentName}`
                });
                const now = new Date();
                let years = 1;
                if (type === "threeYear") {
                    years = 3;
                }
                const subscription_end = now.setFullYear(now.getFullYear() + years);
                const order = await new Subscription({
                    parentsID: _id,
                    parentName: parent.parentName,
                    childName: parent.childrenName,
                    wristband: parent.wristband,
                    subscription_end,
                    stripeResponse
                }).save();

                const child = parent.children.id(childId);
                child.subscription_end = moment(child.subscription_end).add(years, 'year');
                parent.save((error, user) => {
                    if (error) {
                        console.log('error saving ', error);
                        return res.status(400).json({code: error.code, message: "An error occured while saving data into the database!" });
                    }
                    order.childID = child._id;
                    order.save();
                    return res.status(200).send({last4: parent.card.last4, brand: parent.card.brand, orderNo: order.orderNo});
                });
            } catch (err) {
                console.log('error is: ', err.message);
            }
    });
    app.post('/mobile/addchildren', verifyToken, (req, res) => {
        upload(req, res, async (err) => {
            if (err) {
                console.log('multer error', err);
                return res.status(419).json({message: 'Checksum failed'});
            }
            const { _id, parentName } = req.user.user;
            const {
                childrenName,
                wristband,
                stripeToken
               } = req.body;

            if (!stripeToken) {
                return res.status(400).send("No card number provided!");
            }
            try {
                const stripeResponse = await stripe.charges.create({
                    amount: 77600,
                    currency: 'nok',
                    source: stripeToken,
                    description: 'Subscription for a new user'
                });
                const now = new Date();
                const subscription_end = now.setFullYear(now.getFullYear() + 1);
                const order = await new Subscription({
                    parentsID: _id,
                    parentName,
                    childName: childrenName,
                    wristband,
                    subscription_end,
                    stripeResponse
                }).save();
                const user = await Parent.findById({_id});
                if (user) {
                    if (!user.active) {
                        return res.status(401).send("User is deactivated");
                    }
                    const newChild = {
                        childrenName,
                        wristband,
                        image: req.file.filename || "No Image Provided",
                        orderId: order._id,
                        subscription_end
                    };
                    user.children.push(newChild);
                    user.status = "USER";
                    user.save((error, user) => {
                        if (error) {
                            console.log('error saving ', error);
                            return res.status(400).json({code: error.code, message: "An error occured while saving data into the database!" });
                        }
                        const child = _.find(user.children, {wristband: newChild.wristband});
                        order.childID = child._id;
                        order.save();
                        return res.status(200).send({orderNo: order.orderNo});
                    });
                }
            } catch (err) {
                console.log('error is: ', err.message);
            }
        });
    });

    app.post('/mobile/subscriptions', verifyToken, async (req, res) => {
        const { _id } = req.user.user;
        const today = moment();
        try {
            const {children} = await Parent.findById({_id});
            const newChildren = children.map(child => (
                    {
                        _id: child._id,
                        childrenName: child.childrenName,
                        wristband: child.wristband,
                        active: child.active,
                        days_left: moment(child.subscription_end).diff(today, 'days'),
                        expirationDate: moment(child.subscription_end).format('YYYY-MM-DD')
                    }));
            return res.status(200).send({children: newChildren});
        } catch (err) {
            console.log('error fetching subs', err);
            return res.send(400).send({err});
        }
    });

    app.post('/auth/mobile/updateChild', verifyToken, async (req, res) => {
        const { _id } = req.user.user;
        // const _id = '5a7f5fce95616b0decd7d454'
        // const childId = '5a804c001a9b75230208e5a3'
        const {password, childId, childrenName, wristband} = req.body;

        await Parent.findById({_id}, (err, user) => {
            if (!user || err) return res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
            user.comparePassword(password, async (err, isMatched) => {
              if (err) res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
              if (!isMatched) {
                return res.status(401).json({ message: "Wrong Password!"});
              }
                console.log('password matched');

                await Parent.findOneAndUpdate(
                    {_id, "children._id": childId},
                    {
                        "children.$.childrenName": childrenName,
                        "children.$.wristband": wristband
                    }
                );
            return res.send(user);
            });
        });
    });

    app.post('/mobile/invitegaurdian', verifyToken, async (req, res) => {
        const { _id } = req.user.user; // id pf parent
        const { gaurdianName, gaurdianEmail } = req.body;
        const newGaurdian = {
            gaurdianName,
            gaurdianEmail,
        };
        // verify gaurdians are less than 4
        const parents = await Parent.findById({_id});
        if (parents.gaurdians.length >= 4) {
            return res.status(400).send({message: "Limit reached. You can't add more than four gaurdians!"});
        }
        const sendEmail = (email, parent) => {
            const msg = {
              to: email,
              from: 'bel.nabhani@gmail.com',
              subject: `Invitation from ${parent.parentName} to be a guardian`,
              html: `<html>
                      <body>
                        <h2>Kinder-id</h2>
                        <h4>You have been invited to be a guardian to ${parent.parentName}'s children</h4>
                        <h4>Please download the app from Google Play or itunes app store</h4>
                      </body>
                    </html>`
            };
            sgMail.send(msg);
          };

        const saveGaurdian = async (err, parent) => {
            if (gaurdianIsParent) {
                newGaurdian.gaurdianRefId = gaurdianIsParent._id;
                newGaurdian._gaurdianPushNotificationID = gaurdianIsParent.pushNotificationID;
            }

            const user = await Parent.findOneAndUpdate(
                {_id},
                {$push: { gaurdians: newGaurdian}},
                {safe: true, upsert: true},
            );
            sendEmail(gaurdianEmail, user);
        };

        const gaurdianIsParent = await Parent.findOne({email: gaurdianEmail});
        if (gaurdianIsParent) {
            const arr = gaurdianIsParent.invitingParentIds;
            arr.push(_id);
            gaurdianIsParent.invitingParentIds = arr;
            gaurdianIsParent.save(saveGaurdian);
            return res.status(200).send({ message: "Invitation sent Successfully!"});
        }

        const verificationCode = Math.floor(Math.random() * 900000) + 100000;
        const arr = [_id];
        await new Parent({
            email: gaurdianEmail,
            status: "INVITED",
            invitingParentIds: arr,
            verificationCode
        }).save(saveGaurdian);
        return res.status(200).send({ message: "Invitation sent Successfully!"});
        // TODO send email
        // const sendEmail = user => {
        //     const msg = {
        //       to: user.email,
        //       from: 'bel.nabhani@gmail.com',
        //       subject: `Invitation from ${user.parentName} to be a guardian`,
        //       html: `<html>
        //               <body>
        //                 <h2>Kinder-id</h2>
        //                 <h4>You have been invited to be a guardian to ${user.parentName}'s children</h4>
        //                 <h4>Please download the app from Google Play or itunes app store</h4>
        //               </body>
        //             </html>`
        //     };
        //     sgMail.send(msg, (err) => { //only for testing, remove after you finish
        //       if (err) {
        //         console.log('error', err);
        //         return res.status(406).send("Error Sending Email");
        //       }
        //       console.log('email sent');
        //     //   return res.status(200).json({ email: user.email, status: user.status });
        //       return res.status(200).send({ message: "Invitation sent Successfully!"});
        //     }); //only for testing, remove after you finish
        // };
    });

    app.post('/mobile/invitationresponse', verifyToken, async (req, res) => {
        const { _id } = req.user.user; // id of gaurdian

        const { parentId, response } = req.body;
        console.log('body', req.body);

        const gaurdian = await Parent.findOneAndUpdate(
            {_id},
            {$pull: {invitingParentIds: parentId}}
        );
        console.log('gaurdian', gaurdian);

        // update the parents with the gaurdian's response
        const parent = await Parent.findOneAndUpdate(
            {_id: parentId, "gaurdians.gaurdianRefId": _id},
            {"gaurdians.$.inviteStatus": response}
        );
        console.log('parent', parent);

        try {
            const { data } = await axios({
                method: "post",
                headers: {
                    Authorization: "Basic MGJlOWVhNjktMzk0OS00ZTg4LWI4OWYtYWRmMzNhNDEwODIy",
                    "Content-Type": "application/json"
                },
                url: "notifications",
                data: {
                    app_id: "7df6e70b-b433-414b-953a-01d01a377089",
                    // included_segments: ["All"],
                    // data: { foo: "bar" },
                    include_player_ids: [parent.pushNotificationID],
                    contents: {
                        en: `${gaurdian.parentName} has ${response} you invitation to be a guardian.`
                    },
                },
                baseURL: "https://onesignal.com/api/v1/"
            });
            console.log('axios res', data);
            return res.send('OK');
        } catch (errorAxios) {
            console.log('axios error', errorAxios.response.data);
        }
    });
    app.post('/mobile/assigngaurdian', verifyToken, async (req, res) => {
        const { _id } = req.user.user; // id of parent

        const { gaurdianId, gaurdianName, childrenArray } = req.body;
        const children = JSON.parse(childrenArray);
        console.log('children', children);

        await Parent.findOneAndUpdate(
            {_id, "gaurdians._id": gaurdianId},
            {$set: {
                "gaurdians.$.gaurdianName": gaurdianName,
                "gaurdians.$.children": children
                }
            }
        );
        res.status(200).send("OK");
    });

    app.post('/mobile/updateprofile', verifyToken, async (req, res) => {
        const { _id } = req.user.user; // id of parent
        const {password, newPassword, parentName, mobile, email, address, country, zipCode, postalCode} = req.body;
        console.log('zipCode,', zipCode);

        let passwordChanged = false;
        await Parent.findById({_id}, (err, user) => {
            if (!user || err) return res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
            user.comparePassword(password, async (err, isMatched) => {
              if (err) res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
              if (!isMatched) {
                return res.status(401).json({ message: "Wrong Password!"});
              }
              if (newPassword !== null) {
                user.password = newPassword;
                passwordChanged = true;
              }
              user.parentName = parentName;
              user.mobile = mobile;
              user.email = email;
              user.address = address;
              user.country = country;
              user.zipCode = zipCode;
              user.postalCode = postalCode;
              user.save(saveError => {
                  if (saveError) {
                      console.log('save error', saveError);

                      return res.status(400).send({message: "Error saving user!"});
                  }
                  return res.send({passwordChanged});
              });
            });
        });
    });
    app.post('/mobile/deleteprofile', verifyToken, async (req, res) => {
        const { _id } = req.user.user; // id of parent
        const {password, newPassword, parentName, mobile, email, address, country, postalCode} = req.body;
        const passwordChanged = false;
        await Parent.findById({_id}, (err, user) => {
            if (!user || err) return res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
            user.comparePassword(password, async (err, isMatched) => {
              if (err) res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
              if (!isMatched) {
                return res.status(401).json({ message: "Wrong Password!"});
              }
              try {
                await Parent.findOneAndRemove({_id});
                return res.send("OK");
              } catch (removeError) {
                return res.status(400).send("Error deleting profile.");
              }
            });
        });
    });

    app.post('/mobile/deletegaurdian', verifyToken, async (req, res) => {
        const { _id } = req.user.user; // id of parent

        const { gaurdianId } = req.body;

        const parent = await Parent.findById({_id});
        parent.gaurdians.pull(gaurdianId);
        parent.save();

        res.status(200).send("OK");
    });

    app.post('/auth/mobile/validatepassword', verifyToken, async (req, res) => {
        const { _id } = req.user.user;
        const {password} = req.body;
        if (!password) {
            return res.status(400).send("No password provided!");
        }
        await Parent.findById({_id}, (err, user) => {
            if (!user || err) return res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
            user.comparePassword(password, async (err, isMatched) => {
              if (err) res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
              if (!isMatched) {
                return res.status(401).json({ message: "Wrong Password!"});
              }
            return res.status(200).send("OK");
            });
        });
    });
    app.post('/auth/mobile/replaceid', verifyToken, async (req, res) => {
        const { _id } = req.user.user;
        const { childId, color, stripeToken, payingWithCard } = req.body;
        console.log('incoming replace id body', req.body);

        try {
            if (payingWithCard === true) {
                console.log('with card');

                const parent = await Parent.findById({_id});
                console.log('card id', parent.card.id);

                await stripe.charges.create({
                    amount: 4900,
                    currency: 'nok',
                    customer: parent.card.id,
                    description: `Subscription for ${parent.parentName}`
                });
            } else {
                console.log('without card');

                await stripe.charges.create({
                    amount: 4900,
                    currency: 'nok',
                    source: stripeToken,
                    description: `Replace lost wristband ID for ${childId}`
                });
            }
            await Parent.findOneAndUpdate(
                {_id, "children._id": childId },
                {
                    "children.$.wristband": null,
                    "children.$.color": color
                }
            );
            return res.status(200).send("OK");
        } catch (err) {
            console.log('error replaceing id ', err);
            return res.status(400).send(err);
        }
    });

    app.post('/auth/mobile/deleteChildren', verifyToken, async (req, res) => {
        const { _id } = req.user.user;
        const {password, childId } = req.body;

        await Parent.findById({_id}, (err, user) => {
            if (!user || err) return res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
            user.comparePassword(password, async (err, isMatched) => {
                if (err) res.status(401).json({ authorized: false, message: "Something went wrong! Please re-try later" });
                if (!isMatched) {
                    return res.status(401).json({ message: "Wrong Password!"});
                }
                user.children.pull({_id: childId});
                user.save();
                return res.send(user);
            });
        });
    });

    app.post('/mobile/getpushnotificationid', async (req, res) => {
        const { childWristband } = req.body;
        console.log('==============wristband', childWristband);

         await Parent.findOne({
            'children.wristband': childWristband
        }, (err, user) => {
            console.log('===========user', user);

            if (err) {
                return res.status(200).send({error: err});
            }
            if (user) {
                const data = {};
                const child = _.find(user.children, {wristband: childWristband});
                data.parentId = user._id;
                data.parentName = user.parentName;
                data.parentMobile = user.mobile;
                data.gaurdians = user.gaurdians;
                data.childName = child.childrenName;
                data.pushNotificationID = user.pushNotificationID;
                return res.status(200).json(data);
            }
            return res.status(400).send("User not found");
        });
    });
};
