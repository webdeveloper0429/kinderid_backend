const mongoose = require('mongoose');
const path = require('path');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const stripe = require('stripe')('sk_test_05VBjhFZ65fonp5ceecWwct7');
const multer = require('multer');
const _ = require('lodash');
const moment = require('moment');
const verifyToken = require('../../middleware/verifyToken');


const storage = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, path.resolve(__dirname, './temp/'));
    },
    filename(req, file, cb) {
        cb(null, `${file.originalname}-${Date.now()}.jpg`);
      }
});
const upload = multer({ storage }).single('avatar');
const Parent = mongoose.model('Parent');
const Subscription = mongoose.model('Subscription');

module.exports = app => {
    app.post('/mobile/payandregisteruser', async (req, res) => {
        upload(req, res, async (err) => {
            if (err) {
                console.log('multer error', err);
                return res.status(419).json({message: 'Checksum failed'});
            }
            const {
                email,
                children,
                stripeToken
               } = req.body;
               console.log('body====', req.body);

            if (!stripeToken) {
                return res.status(400).send("No card number provided!");
            }
            try {
                const customer = await stripe.customers.create({
                    email,
                    source: stripeToken,
                });
                console.log('customer created', customer.id);

                const stripeResponse = await stripe.charges.create({
                    amount: 77600,
                    currency: 'nok',
                    customer: customer.id,
                    description: 'Subscription for a new user'
                });
                console.log('payment done', stripeResponse.id);

                const now = new Date();
                const subscription_end = now.setFullYear(now.getFullYear() + 1);
                const order = await new Subscription({
                    subscription_end,
                    stripeResponse
                }).save();
                console.log('stripe completed');

               const parsedChild = JSON.parse(children);
               parsedChild.image = req.file.filename;
               parsedChild.orderId = order._id;
               parsedChild.subscription_end = subscription_end;

                const parent = await Parent.findOne({email});
                if (parent.status === 'USER') {
                    return res.status(401).send("Unauthorized attemp!");
                }
                parent.children.push(parsedChild);
                parent.verificationCode = null;
                parent.card = {
                    id: customer.id,
                    last4: stripeResponse.source.last4,
                    brand: stripeResponse.source.brand
                };
                parent.status = "USER";
                parent.save((error, user) => {
                    if (error) {
                        console.log('error saving ', error);
                        return res.status(400).json({code: error.code, message: "An error occured while saving data!" });
                    }
                    order.parentsID = user._id;
                    order.parentName = user.parentName;
                    const child = _.find(user.children, {wristband: parsedChild.wristband});
                    console.log('child ID', child);

                    order.childID = child._id;
                    order.childName = parsedChild.childrenName;
                    order.wristband = parsedChild.wristband;
                    order.save();
                    const token = jwt.sign({user}, keys.jwtSecret, {expiresIn: '1h'});
                    return res.status(200).json({message: 'new_user_registered', token, orderNo: order.orderNo });
                });
            } catch (error) {
                console.log('error in payment', error);

                return res.status(400).send("Could not save user!");
            }
        });
    });

    // app.post('/mobile/registeruser', (req, res) => {
    //     upload(req, res, async (err) => {
    //         if (err) {
    //             console.log('multer error', err);
    //             return res.status(419).json({message: 'Checksum failed'});
    //         }
    //         const {
    //             email,
    //             parentName,
    //             mobile,
    //             parentPassword,
    //             children,
    //             gaurdians
    //            } = req.body;
    //            console.log('all body ', req.body);

    //            let parsedGaurdians = JSON.parse(gaurdians);
    //            console.log('new gaurdians', parsedGaurdians);

    //            let parsedChild = JSON.parse(children);
    //            parsedChild.image = 'testImage.jpg'//req.file.filename;


    //            const user = await new Parent({
    //                email,
    //                password: parentPassword,
    //                parentName,
    //                mobile,
    //                children: parsedChild,
    //                gaurdians: parsedGaurdians
    //             });

    //             user.save( (error, user) => {
    //                 if (error) {
    //                     console.log('error saving ' ,error);

    //                     return res.status(400).json({code: error.code, message: "An error occured while saving data into the database!" })
    //                 }
    //                 console.log('saved result', user);

    //                 const token = jwt.sign({user}, keys.jwtSecret, {expiresIn: '1h'});
    //                 return res.status(200).json({message: 'new_user_registered', token });
    //             });

    //     });

    // });
    app.post('/mobile/validatewristband', async (req, res) => {
        const { wristband } = req.body;
        if (!wristband) {
            return res.status(400).json({ error: "No wristband provided!"});
        }
        const found = await Parent.findOne({"children.wristband": wristband});
        if (found) {
            return res.status(200).json({uniqueWritband: false});
        }
        return res.status(200).json({uniqueWritband: true});
    });

    app.post('/mobile/registerpushnotification', verifyToken, async (req, res) => {
        const { _id } = req.user.user;
        const { pushNotificationID} = req.body;
        if (!pushNotificationID) {
            return res.status(400).send("Bad Request!");
        }
        try {
            const user = await Parent.findById({_id}).exec();
            user.pushNotificationID = pushNotificationID;
            user.save();
            return res.status(200).json({message: "Push notification ID saved"});
        } catch (err) {
            return res.status(400).send("Push notification ID not saved!");
        }
    });

    app.post('/test/populate', async (req, res) => {
        const _id = "5aa5a08a7bc16b16d495fa5d";
        const today = moment();

        const {children} = await Parent.findById({_id});
        // return res.send(children)
        // let newChildren = children.toJSON();
        newChildren = children.map(child => (
                {
                    _id: child._id,
                    childrenName: child.childName,
                    wristband: child.wristband,
                    active: child.active,
                    days_left: moment(child.subscription_end).diff(today, 'days')
                }));
        return res.json({children: newChildren});
        const {childId} = req.body;
        const user = await Parent.findOne({"children._id": childId}, "children.$");
        console.log('user', user);

        // const child = await Parent.aggregate([
        //     { $unwind: '$children' },
        //     { $unwind: '$children.subscribtion' },
        //     { $match: {'children.wristband' : wristband } },

        //     { $project: {
        //         childName: '$children.childrenName',
        //         wristband: '$children.wristband',

        //     }},

        // ])
        const child = user.children[0].toJSON();
        child.days_left = moment(child.subscription_end).diff(moment(), 'days');
        console.log(child);


        return res.json(child);
        if (children) {
                  const subscriptions = await Subscription.findOne({_id: "5aa8465cff3a862259825040"});

            console.log('subsc', subscriptions);
            return res.json(subscriptions);
        }

        // .findOne({_id, "children.wristband": wristband});
        // if (children) {
        //     const subscriptions = children.map(sub => {
        //         return  sub.subscribtion });
        //     console.log('subsc', subscriptions);

        // }
        // if (child) {
        //     const subscribtion = await Subscribtion.findOne({orderID: child.subscribtion})

        //     console.log(subscribtion);

        //     return res.json({subscribtion});
        // }
    });
}
;
