const mongoose = require('mongoose');
const keys = require('../config/keys');

const Subscribers = mongoose.model('subscribers');


module.exports = app => {
 app.post('/api/subscribe', async (req, res) => {  
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   if (req.body.email == null) return res.status(400).json({message: "error"});
   if (req.body.email == 0) return res.status(400).json({message: "No Email Entered"});
   const { email } = req.body;
   try{
     await new Subscribers({ 
       email,
       created_at: Date.now() 
      }).save();
     return res.status(200).json({message: "Thank you for your interest, you will be the first to receive more information about KinderID"});
   } catch (error) {
     return res.status(400).json({message: error})
   }
 });
};
