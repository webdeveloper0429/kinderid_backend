const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');
const stripe = require('stripe')('sk_test_05VBjhFZ65fonp5ceecWwct7');
const verifyToken = require('../middleware/verifyToken');


const Parents = mongoose.model('Parent');
const Subscription = mongoose.model('Subscription');

module.exports = app => {
    app.post('/payment/newcustomer', async (req, res) => {
         const {tokenId} = req.body;
         if (!tokenId) {
             return res.status(400).send("No card number provided!");
         }
         console.log('incoming token ', tokenId);
         try {
             const charge = await stripe.charges.create({
                 amount: 77600,
                 currency: 'nok',
                 source: tokenId,
                 description: 'Subscription for one year'
             });
             console.log('response from stripe', charge);
             await new Subscription({
                 parentsID: String,
                 childID: String,
                 subscribtion_start: Date,
                 subscribtion_end: Date,
                 wristbandID: String,
             });
         } catch (err) {
             console.log('error ', err);
         }
     });

    app.post('/payment/oneyear', async (req, res) => {
        const {tokenId} = req.body;
        if (!tokenId) {
            return res.status(400).send("No card number provided!");
        }
        console.log('incoming token ', tokenId);
        try {
            const charge = await stripe.charges.create({
                amount: 24900,
                currency: 'nok',
                source: tokenId,
                description: 'Subscription for one year'
            });
            console.log('response from stripe', charge);
        } catch (err) {
            console.log('error ', err);
        }
    });

    app.post('/payment/threeyears', async (req, res) => {
        const {tokenId} = req.body;
        if (!tokenId) {
            return res.status(400).send("No card number provided!");
        }
        console.log('incoming token ', tokenId);
        try {
            const charge = await stripe.charges.create({
                amount: 49900,
                currency: 'nok',
                source: tokenId,
                description: 'Subscription for one year'
            });
            console.log('response from stripe', charge);
        } catch (err) {
            console.log('error ', err);
        }
    });
};
