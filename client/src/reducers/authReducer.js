import { FETCH_USER, EMAIL_LOGIN } from '../actions/types';
const INITIAL_STATE = {
  user: null,
  email: null
}

export default function(state = INITIAL_STATE, action) {  
  switch (action.type) {
    case FETCH_USER:
      return {user: action.payload || false};
    case EMAIL_LOGIN:
      return {email: action.payload || false, user: "pending"};
    default:
      return state;
  }
}
