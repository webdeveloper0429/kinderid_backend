import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from '../actions';

import SuperAdminDashboard from './SuperAdminDashboard/SuperAdminDashboard'


import EmailLogin from './EmailLogin';
import GoogleLogin from './GoogleLogin';


class App extends Component {

  componentDidMount() {
    this.props.fetchUser();
  }

  render() {
    console.log('the value of email: ',this.props.email)
    console.log('value of user: ', this.props.user)
    switch (true) {
      case this.props.user === null:
        return <div /> ;
      case this.props.user === false:
        return <EmailLogin /> ;
      case this.props.email === "authorized":
      case this.props.user === "pending":
        return <GoogleLogin />;        
      default:
        return <SuperAdminDashboard />
    }
  }
}

function mapStateToProps({auth}) {
  const { user, email } = auth;
  return { user, email };
}
export default connect(mapStateToProps, actions)(App);