import React from 'react';

const Footer = () => {
  return (
    <div>
      <footer className="main-footer">
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-6">
              <p>Webstack &copy; 2017-2019</p>
            </div>
            <div className="col-sm-6 text-right">
              <p>
                Design by{' '}
                <a href="https://webstack.no" className="external">
                  Webstack
                </a>
              </p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
