import React, { Component } from 'react';
import axios from 'axios';

class Armband extends Component {
    constructor(props) {
        super(props);
        this.state = { message: ''}
    }
    
  handelSubmit = async (e) => {
    e.preventDefault();
    const prefix = this.refs.prefix.value;
    const {data} = await axios.post('/api/insert/newstock', {prefix})
    if (data) {
        this.setState({message: data.message})
    }
  }
  render() {
    return (
      <div className="page home-page d-flex align-items-center h-100">
      <div className="container" >
      <div className="row justify-content-center ">
        <div className="col-lg-5">
          <div className="card ">
            <div className="card-header d-flex align-items-center">
              <h2 className="h5 display">Generate IDs</h2>
            </div>
            <div className="card-body">
              <form onSubmit={this.handelSubmit} className="form-inline">
                <div className="form-group">
                  <label htmlFor="inlineFormInput" className="sr-only">
                    Prefix
                  </label>
                  <input
                    id="inlineFormInput"
                    ref="prefix"
                    type="text"
                    placeholder="AB"
                    className="mx-sm-1 form-control"
                    required="true"
                  />
                </div>
                <div className="form-group">
                  <input
                    type="submit"
                    value="Generate"
                    className="mx-sm-3 btn btn-primary"
                  />
                </div>
                <div className="form-group">
                  {this.state.message}
                </div>
              </form>
            </div>
          </div>
          </div>
        </div>
        </div>
      </div>
    );
  }
}

export default Armband;
