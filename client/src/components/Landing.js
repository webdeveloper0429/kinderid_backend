import React from 'react';

const Landing = () => {
  return (
    <div className="page home-page">
      <section className="dashboard-counts section-padding">
        <div className="container-fluid">
          <div className="row">
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="fa fa-users" aria-hidden="true"></i></div>
                <div className="name"><strong className="text-uppercase">Nye kunder</strong><span>i siste 7 dager</span>
                  <div className="count-number">25</div>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="fa fa-users" aria-hidden="true"></i></div>
                <div className="name"><strong className="text-uppercase">Aktive brukere</strong><span>Nå</span>
                  <div className="count-number">25</div>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="icon-padnote"></i></div>
                <div className="name"><strong className="text-uppercase">Dagens salg</strong><span>i dag</span>
                  <div className="count-number">40</div>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="icon-check"></i></div>
                <div className="name"><strong className="text-uppercase">Måndelig salg</strong><span>gjennomsnitt</span>
                  <div className="count-number">342</div>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="icon-bill"></i></div>
                <div className="name"><strong className="text-uppercase">New Invoices</strong><span>Last 2 days</span>
                  <div className="count-number">123</div>
                </div>
              </div>
            </div>
            
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="icon-list-1"></i></div>
                <div className="name"><strong className="text-uppercase">Available armand</strong><span>In Stock</span>
                  <div className="count-number">700</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="dashboard-header section-padding">
        <div className="container-fluid">
          <div className="row d-flex align-items-md-stretch">
  
            <div className="col-lg-3 col-md-6">
              <div className="wrapper to-do">
                {/* <header>
                  <h2 className="display h4">To do List</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </header>
                <ul className="check-lists list-unstyled">
                  <li className="d-flex align-items-center"> 
                    <input type="checkbox" id="list-1" name="list-1" className="form-control-custom"/>
                    <label htmlFor="list-1">Similique sunt in culpa qui officia</label>
                  </li>
                  <li className="d-flex align-items-center"> 
                    <input type="checkbox" id="list-2" name="list-2" className="form-control-custom"/>
                    <label htmlFor="list-2">Ed ut perspiciatis unde omnis iste</label>
                  </li>
                  <li className="d-flex align-items-center"> 
                    <input type="checkbox" id="list-3" name="list-3" className="form-control-custom"/>
                    <label htmlFor="list-3">At vero eos et accusamus et iusto </label>
                  </li>
                  <li className="d-flex align-items-center"> 
                    <input type="checkbox" id="list-4" name="list-4" className="form-control-custom"/>
                    <label htmlFor="list-4">Explicabo Nemo ipsam voluptatem</label>
                  </li>
                  <li className="d-flex align-items-center"> 
                    <input type="checkbox" id="list-5" name="list-5" className="form-control-custom"/>
                    <label htmlFor="list-5">Similique sunt in culpa qui officia</label>
                  </li>
                  <li className="d-flex align-items-center"> 
                    <input type="checkbox" id="list-6" name="list-6" className="form-control-custom"/>
                    <label htmlFor="list-6">At vero eos et accusamus et iusto </label>
                  </li>
                  <li className="d-flex align-items-center"> 
                    <input type="checkbox" id="list-7" name="list-7" className="form-control-custom"/>
                    <label htmlFor="list-7">Similique sunt in culpa qui officia</label>
                  </li>
                  <li className="d-flex align-items-center"> 
                    <input type="checkbox" id="list-8" name="list-8" className="form-control-custom"/>
                    <label htmlFor="list-8">Ed ut perspiciatis unde omnis iste</label>
                  </li>
                </ul> */}
              </div>
            </div>
 
            <div className="col-lg-3 col-md-6">
              <div className="wrapper project-progress">
                <h2 className="display h4">Most Sales Cities</h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                <div className="pie-chart">
                  <canvas id="pieChart"></canvas>
                </div>
              </div>
            </div>

            <div className="col-lg-6 col-md-12 flex-lg-last flex-md-first align-self-baseline">
              <div className="wrapper sales-report">
                <h2 className="display h4">Sales marketing report</h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor amet officiis</p>
                <div className="line-chart">
                  <canvas id="lineCahrt"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="statistics section-padding section-no-padding-bottom">
        <div className="container-fluid">
          <div className="row d-flex align-items-stretch">
            <div className="col-lg-4">
     
              <div className="wrapper income text-center">
                <div className="icon"><i className="icon-line-chart"></i></div>
                <div className="number">126,418</div><strong className="text-primary">All Income</strong>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do.</p>
              </div>
            </div>
            <div className="col-lg-4">

              <div className="wrapper data-usage">
                <h2 className="display h4">Monthly Usage</h2>
                <div className="row d-flex align-items-center">
                  <div className="col-sm-6">
                    <div id="progress-circle" className="d-flex align-items-center justify-content-center"></div>
                  </div>
                  <div className="col-sm-6"><strong className="text-primary">80.56 Gb</strong><small>Current Plan</small><span>100 Gb Monthly</span></div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
              </div>
            </div>
            <div className="col-lg-4">
  
              <div className="wrapper user-activity">
                <h2 className="display h4">User Activity</h2>
                <div className="number">210</div>
                <h3 className="h4 display">Social Users</h3>
                <div className="progress">
                  <div role="progressbar" style={{width: "25%"}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" className="progress-bar progress-bar bg-primary"></div>
                </div>
                <div className="page-statistics d-flex justify-content-between">
                  <div className="page-visites"><span>Pages Visites</span><strong>230</strong></div>
                  <div className="new-visites"><span>New Visites</span><strong>73.4%</strong></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    
      <section className="updates section-padding">
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-4 col-md-12">
    
              <div id="new-updates" className="wrapper recent-updated">
                <div id="updates-header" className="card-header d-flex justify-content-between align-items-center">
                  <h2 className="h5 display"><a data-toggle="collapse" data-parent="#new-updates" href="#updates-box" aria-expanded="true" aria-controls="updates-box">News Updates</a></h2><a data-toggle="collapse" data-parent="#new-updates" href="#updates-box" aria-expanded="true" aria-controls="updates-box"><i className="fa fa-angle-down"></i></a>
                </div>
                <div id="updates-box" role="tabpanel" className="collapse show">
                  <ul className="news list-unstyled">
              
                    <li className="d-flex justify-content-between"> 
                      <div className="left-col d-flex">
                        <div className="icon"><i className="icon-rss-feed"></i></div>
                        <div className="title"><strong>Lorem ipsum dolor sit amet.</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                      </div>
                      <div className="right-col text-right">
                        <div className="update-date">24<span className="month">Jan</span></div>
                      </div>
                    </li>
             
                    <li className="d-flex justify-content-between"> 
                      <div className="left-col d-flex">
                        <div className="icon"><i className="icon-rss-feed"></i></div>
                        <div className="title"><strong>Lorem ipsum dolor sit amet.</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                      </div>
                      <div className="right-col text-right">
                        <div className="update-date">24<span className="month">Jan</span></div>
                      </div>
                    </li>
              
                    <li className="d-flex justify-content-between"> 
                      <div className="left-col d-flex">
                        <div className="icon"><i className="icon-rss-feed"></i></div>
                        <div className="title"><strong>Lorem ipsum dolor sit amet.</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                      </div>
                      <div className="right-col text-right">
                        <div className="update-date">24<span className="month">Jan</span></div>
                      </div>
                    </li>
       
                    <li className="d-flex justify-content-between"> 
                      <div className="left-col d-flex">
                        <div className="icon"><i className="icon-rss-feed"></i></div>
                        <div className="title"><strong>Lorem ipsum dolor sit amet.</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                      </div>
                      <div className="right-col text-right">
                        <div className="update-date">24<span className="month">Jan</span></div>
                      </div>
                    </li>
     
                    <li className="d-flex justify-content-between"> 
                      <div className="left-col d-flex">
                        <div className="icon"><i className="icon-rss-feed"></i></div>
                        <div className="title">
                          <h3 className="h5">Lorem ipsum dolor sit amet.</h3>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                      </div>
                      <div className="right-col text-right">
                        <div className="update-date">24<span className="month">Jan</span></div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6">
              <div id="daily-feeds" className="wrapper daily-feeds">
                <div id="feeds-header" className="card-header d-flex justify-content-between align-items-center">
                  <h2 className="h5 display"><a data-toggle="collapse" data-parent="#daily-feeds" href="#feeds-box" aria-expanded="true" aria-controls="feeds-box">Your daily Feeds </a></h2>
                  <div className="right-column">
                    <div className="badge badge-primary">10 messages</div><a data-toggle="collapse" data-parent="#daily-feeds" href="#feeds-box" aria-expanded="true" aria-controls="feeds-box"><i className="fa fa-angle-down"></i></a>
                  </div>
                </div>
                <div id="feeds-box" role="tabpanel" className="collapse show">
                  <div className="feed-box">
                    <ul className="feed-elements list-unstyled">

                      <li className="clearfix">
                        <div className="feed d-flex justify-content-between">
                          <div className="feed-body d-flex justify-content-between"><a href="/" className="feed-profile"><img src="img/avatar-5.jpg" alt="person" className="img-fluid rounded-circle"/></a>
                            <div className="content"><strong>Aria Smith</strong><small>Posted a new blog </small>
                              <div className="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                            </div>
                          </div>
                          <div className="date"><small>5min ago</small></div>
                        </div>
                      </li>
       
                      <li className="clearfix">
                        <div className="feed d-flex justify-content-between">
                          <div className="feed-body d-flex justify-content-between"><a href="/" className="feed-profile"><img src="img/avatar-2.jpg" alt="person" className="img-fluid rounded-circle"/></a>
                            <div className="content"><strong>Frank Williams</strong><small>Posted a new blog </small>
                              <div className="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                              <div className="CTAs"><a href="/" className="btn btn-xs btn-dark"><i className="fa fa-thumbs-up"> </i>Like</a><a href="/" className="btn btn-xs btn-dark"><i className="fa fa-heart"> </i>Love</a></div>
                            </div>
                          </div>
                          <div className="date"><small>5min ago</small></div>
                        </div>
                      </li>
                
                      <li className="clearfix">
                        <div className="feed d-flex justify-content-between">
                          <div className="feed-body d-flex justify-content-between"><a href="/" className="feed-profile"><img src="img/avatar-3.jpg" alt="person" className="img-fluid rounded-circle"/></a>
                            <div className="content"><strong>Ashley Wood</strong><small>Posted a new blog </small>
                              <div className="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                            </div>
                          </div>
                          <div className="date"><small>5min ago</small></div>
                        </div>
                      </li>
              
                      <li className="clearfix">
                        <div className="feed d-flex justify-content-between">
                          <div className="feed-body d-flex justify-content-between"><a href="/" className="feed-profile"><img src="img/avatar-1.jpg" alt="person" className="img-fluid rounded-circle"/></a>
                            <div className="content"><strong>Jason Doe</strong><small>Posted a new blog </small>
                              <div className="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                            </div>
                          </div>
                          <div className="date"><small>5min ago</small></div>
                        </div>
                        <div className="card"> <small>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</small></div>
                        <div className="CTAs pull-right"><a href="/" className="btn btn-xs btn-dark"><i className="fa fa-thumbs-up"> </i>Like</a></div>
                      </li>
              
                      <li className="clearfix">
                        <div className="feed d-flex justify-content-between">
                          <div className="feed-body d-flex justify-content-between"><a href="/" className="feed-profile"><img src="img/avatar-6.jpg" alt="person" className="img-fluid rounded-circle"/></a>
                            <div className="content"><strong>Sam Martinez</strong><small>Posted a new blog </small>
                              <div className="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                            </div>
                          </div>
                          <div className="date"><small>5min ago</small></div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6">
              <div id="recent-activities-wrapper" className="wrapper recent-activities">
                <div id="activites-header" className="card-header d-flex justify-content-between align-items-center">
                  <h2 className="h5 display"><a data-toggle="collapse" data-parent="#recent-activities-wrapper" href="#activities-box" aria-expanded="true" aria-controls="activities-box">Recent Activities</a></h2><a data-toggle="collapse" data-parent="#recent-activities-wrapper" href="#activities-box" aria-expanded="true" aria-controls="activities-box"><i className="fa fa-angle-down"></i></a>
                </div>
                <div id="activities-box" role="tabpanel" className="collapse show">
                  <ul className="activities list-unstyled">
                 
                    <li>
                      <div className="row">
                        <div className="col-4 date-holder text-right">
                          <div className="icon"><i className="icon-clock"></i></div>
                          <div className="date"> <span>6:00 am</span><span className="text-info">6 hours ago</span></div>
                        </div>
                        <div className="col-8 content"><strong>Meeting</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                        </div>
                      </div>
                    </li>
                
                    <li>
                      <div className="row">
                        <div className="col-4 date-holder text-right">
                          <div className="icon"><i className="icon-clock"></i></div>
                          <div className="date"> <span>6:00 am</span><span className="text-info">6 hours ago</span></div>
                        </div>
                        <div className="col-8 content"><strong>Meeting</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                        </div>
                      </div>
                    </li>
                
                    <li>
                      <div className="row">
                        <div className="col-4 date-holder text-right">
                          <div className="icon"><i className="icon-clock"></i></div>
                          <div className="date"> <span>6:00 am</span><span className="text-info">6 hours ago</span></div>
                        </div>
                        <div className="col-8 content"><strong>Meeting</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                        </div>
                      </div>
                    </li>
            
                    <li>
                      <div className="row">
                        <div className="col-4 date-holder text-right">
                          <div className="icon"><i className="icon-clock"></i></div>
                          <div className="date"> <span>6:00 am</span><span className="text-info">6 hours ago</span></div>
                        </div>
                        <div className="col-8 content"><strong>Meeting</strong>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Landing;
