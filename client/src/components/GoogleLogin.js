import React, {Component} from "react";

class googleLogin extends Component{  
  render () {
  return ( 
    <div className="page login-page">
      <div className="page home-page d-flex align-items-center h-100">
      <div className="container" >
      <div className="row justify-content-center ">
        <div className="col-lg-5">
          <div className="card ">
            <div className="card-header d-flex align-items-center">
              <strong className="text-primary">kinder id</strong>
            </div>
            <div className="card-body">
              <a href="/auth/google" className="btn btn-primary">
              <div className="icon"><i class="fa fa-google-plus" aria-hidden="true"></i></div>Login with Google</a>
            </div>
          </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  );
};
}
export default googleLogin;
