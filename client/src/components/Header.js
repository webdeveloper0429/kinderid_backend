import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import { connect } from 'react-redux';

class Header extends Component {

  render() {    
    return (
      <div>
        <nav className="side-navbar">
            <div className="side-navbar-wrapper">
                <div className="sidenav-header d-flex align-items-center justify-content-center">
                    <div className="sidenav-header-inner text-center">
                        <img src={this.props.auth.imageURL} alt="person" className="img-fluid rounded-circle" />
                        <h2 className="h5 text-uppercase">{this.props.auth.fullName} </h2>
                        {/* <span className="text-uppercase">{this.props.auth.role}</span> */}
                    </div>
                    <div className="sidenav-header-logo">
                        <NavLink to="/" className="brand-small text-center">
                            <strong>K</strong>
                            <strong className="text-primary">I</strong>
                        </NavLink>
                    </div>
                </div>
                <div className="main-menu">
                    <ul id="side-main-menu" className="side-menu list-unstyled">
                        <li>
                            <NavLink to="/">
                                <i className="icon-home" aria-hidden="true"></i>
                                <span>Hjem</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to="/users">
                              <i className="fa fa-users" aria-hidden="true"></i>
                                <span>Brukere</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to="/interal">
                              <i className="fa fa-building-o" aria-hidden="true"></i>
                              <span>Intern Data</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to="orders">
                              <i className="fa fa-shopping-bag" aria-hidden="true"></i>
                              <span>Nettbutikk</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to="/notification">
                              <i className="fa fa-commenting-o" aria-hidden="true"></i>
                              <span>Push Varsel</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to="/settings">
                              <i className="fa fa-cogs" aria-hidden="true"></i>
                              <span>Innstillinger</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to="/armband">
                              <i className="fa fa-circle-o-notch" aria-hidden="true"></i>
                              <span>Armbånd</span>
                            </NavLink>
                        </li>                              
                    </ul>
                </div>
            </div>
        </nav>

    <header className="header">
        <nav className="navbar">
            <div className="container-fluid">
                <div className="navbar-holder d-flex align-items-center justify-content-between">
                    <div className="navbar-header">
                        <NavLink id="toggle-btn" to="/" className="menu-btn">
                            <i className="icon-bars"> </i>
                        </NavLink>
                        <NavLink to="/" className="navbar-brand">
                            <div className="brand-text d-none d-md-inline-block">
                                <strong className="text-primary">kinder id </strong>
                                <span>Dashboard</span>
                            </div>
                        </NavLink>
                    </div>
                    <ul className="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                        <li className="nav-item dropdown">
                            <NavLink id="notifications" rel="nofollow" data-target="/" to="/" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" className="nav-link">
                                <i className="fa fa-bell"></i>
                                <span className="badge badge-warning">12</span>
                            </NavLink>
                            <ul aria-labelledby="notifications" className="dropdown-menu">
                                <li>
                                    <NavLink rel="nofollow" to="/" className="dropdown-item">
                                        <div className="notification d-flex justify-content-between">
                                            <div className="notification-content">
                                                <i className="fa fa-envelope"></i>You have 6 new messages </div>
                                            <div className="notification-time">
                                                <small>4 minutes ago</small>
                                            </div>
                                        </div>
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink rel="nofollow" to="/" className="dropdown-item">
                                        <div className="notification d-flex justify-content-between">
                                            <div className="notification-content">
                                                <i className="fa fa-twitter"></i>You have 2 followers</div>
                                            <div className="notification-time">
                                                <small>4 minutes ago</small>
                                            </div>
                                        </div>
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink rel="nofollow" to="/" className="dropdown-item">
                                        <div className="notification d-flex justify-content-between">
                                            <div className="notification-content">
                                                <i className="fa fa-upload"></i>Server Rebooted</div>
                                            <div className="notification-time">
                                                <small>4 minutes ago</small>
                                            </div>
                                        </div>
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink rel="nofollow" to="/" className="dropdown-item">
                                        <div className="notification d-flex justify-content-between">
                                            <div className="notification-content">
                                                <i className="fa fa-twitter"></i>You have 2 followers</div>
                                            <div className="notification-time">
                                                <small>10 minutes ago</small>
                                            </div>
                                        </div>
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink rel="nofollow" to="/" className="dropdown-item all-notifications text-center">
                                        <strong>
                                            <i className="fa fa-bell"></i>view all notifications </strong>
                                    </NavLink>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item dropdown">
                            <NavLink id="messages" rel="nofollow" data-target="/" to="/" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" className="nav-link">
                                <i className="fa fa-envelope"></i>
                                <span className="badge badge-info">10</span>
                            </NavLink>
                            <ul aria-labelledby="notifications" className="dropdown-menu">
                                <li>
                                    <NavLink rel="nofollow" to="/" className="dropdown-item d-flex">
                                        <div className="msg-profile">
                                            <img src="img/avatar-1.jpg" alt="..." className="img-fluid rounded-circle" />
                                        </div>
                                        <div className="msg-body">
                                            <h3 className="h5">Jason Doe</h3>
                                            <span>sent you a direct message</span>
                                            <small>3 days ago at 7:58 pm - 10.06.2014</small>
                                        </div>
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink rel="nofollow" to="/" className="dropdown-item d-flex">
                                        <div className="msg-profile">
                                            <img src="img/avatar-2.jpg" alt="..." className="img-fluid rounded-circle" />
                                        </div>
                                        <div className="msg-body">
                                            <h3 className="h5">Frank Williams</h3>
                                            <span>sent you a direct message</span>
                                            <small>3 days ago at 7:58 pm - 10.06.2014</small>
                                        </div>
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink rel="nofollow" to="/" className="dropdown-item d-flex">
                                        <div className="msg-profile">
                                            <img src="img/avatar-3.jpg" alt="..." className="img-fluid rounded-circle" />
                                        </div>
                                        <div className="msg-body">
                                            <h3 className="h5">Ashley Wood</h3>
                                            <span>sent you a direct message</span>
                                            <small>3 days ago at 7:58 pm - 10.06.2014</small>
                                        </div>
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink rel="nofollow" to="/" className="dropdown-item all-notifications text-center">
                                        <strong>
                                            <i className="fa fa-envelope"></i>Read all messages </strong>
                                    </NavLink>
                                </li>
                            </ul>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/api/logout" className="nav-link logout">Logg ut
                                <i className="fa fa-sign-out"></i>
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
</div>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
} 


export default connect(mapStateToProps)(Header);
