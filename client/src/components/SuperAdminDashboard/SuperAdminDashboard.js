import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Header from '../Header';
import Landing from '../Landing';
import Footer from '../Footer';
import Users from './Users';
import Internal from './Internal';
import Orders from './Orders';
import Notifications from './Notifications';
import Settings from './Settings';
import Armband from './Armband';

const SuperDashboard = () => {
    return (
        <div>
            <BrowserRouter>
                <div>
                    <Header />            
                    <Route exact path="/" component={Landing} />
                    <Route exact path="/users" component={Users} />
                    <Route exact path="/internal" component={Internal} />
                    <Route exact path="/orders" component={Orders} />
                    <Route exact path="/notification" component={Notifications} />
                    <Route exact path="/settings" component={Settings} />
                    <Route exact path="/armband" component={Armband} />
                    <Footer />            
                </div>
            </BrowserRouter> 
        </div>
    );
};

export default SuperDashboard;