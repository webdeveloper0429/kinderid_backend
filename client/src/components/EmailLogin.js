import React, {Component} from "react";
import {connect} from 'react-redux';
import * as actions from '../actions'

class EmailLogin extends Component{
  handleSubmit = (e) => {
    e.preventDefault();    
    this.props.emailLogin(this.refs.email.value);
  }
  
  render () {
  return (
    <div className="page login-page">
      <div className="container">
        <div className="form-outer text-center d-flex align-items-center">
          <div className="form-inner">
            <div className="logo text-uppercase">
              <strong className="text-primary">kinder id</strong>
            </div>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud.
            </p>
            <form id="login-form"  onSubmit={this.handleSubmit}>
              <div className="form-group">
              
                <label htmlFor="email" className="label-custom">
                  Epost adresse
                </label>
                
                <input
                  id="email"
                  type="email"
                  ref="email"
                  name="loginEmail"
                  required="true"
                />
                <input type="submit" value="Submit" className="btn btn-primary"/>
              </div>
            </form>
          </div>
        </div>
        <div className="copyrights text-center">
          <p>
            Design by{" "}
            <a href="https://webstack.no" className="external">
              Webstack
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};
}
export default connect(null, actions)(EmailLogin);
