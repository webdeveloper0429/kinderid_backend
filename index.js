const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const keys = require('./config/keys');
const bodyParser = require('body-parser');
require('./models/AdminUser');
require('./models/Parent');
require('./models/RegisterParent');
require('./models/Stock');
require('./models/Subscripers');
require('./models/Subscription');
require('./services/passport');

mongoose.connect(keys.mongoURI);

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(
  cookieSession({
    maxAge: 24 * 60 * 60 * 1000,
    keys: [keys.cookieSecret]
  })
);
app.use(passport.initialize());
app.use(passport.session());

require('./routes/authRoutes')(app);
require('./routes/stockRoutes')(app);
require('./routes/mobileRoutes/mobileAuthRoutes')(app);
require('./routes/mobileRoutes/registerRoute')(app);
require('./routes/mobileRoutes/childrenRoutes')(app);
require('./routes/subscribersRoutes')(app);
require('./routes/paymentRoutes')(app);
require('./routes/desktopRoutes/authRoutes')(app);
require('./routes/desktopRoutes/userRoutes')(app);


app.use(express.static('client/build'));

const path = require('path');
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
});

app.listen(5000);
