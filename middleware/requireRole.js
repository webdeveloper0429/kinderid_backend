module.exports = { 
    isSuperAdmin: (req, res, next) => {
        console.log('the role is : ',req.user.role)
        if (req.user.role !== "Super Admin") {
            console.log('no you cant')
            return res.send({ message: 'You are not authorized!'})
        }
        next();
    },
    isAdmin: (req, res, next) => {
        if (!req.user.role !== "admin") {
            return res.status(401).send({ message: 'You are not authorized'})
        }
        next();
    }
}