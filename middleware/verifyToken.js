const jwt = require('jsonwebtoken');
const {jwtSecret} = require('../config/keys')

module.exports = async (req, res, next) => {
  // Get auth header value
  const bearerHeader = req.headers['authorization'];
  // Check if bearer is undefined
  if(typeof bearerHeader !== 'undefined') {
    // Split at the space
    const bearer = bearerHeader.split(' ');
    // Get token from array
    const bearerToken = bearer[1];
    // Set the token
    // req.token = bearerToken;

      jwt.verify(bearerToken, jwtSecret, (err, user) => {
        if (err) {
          if (err.name === "TokenExpiredError") {
            return res.status(403).send({mesasge: err.name} );
          }
          return res.sendStatus(403);
        }
        // delete user.password;
        req.user = user
        next();
      });
  } else {
    return res.status(401).send('No Authorization Header!')
  }
}
