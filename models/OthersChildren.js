const mongoose = require('mongoose');
const { Schema } = mongoose;


const othersChildrenSchema = new Schema({
  childrenName: String,
  wristband: {type: String, unique: true },
  Status: { type: String, default: "Pending" },
  _parent: { type: Schema.Types.ObjectId, ref: 'Parent' }
});


module.exports = othersChildrenSchema;