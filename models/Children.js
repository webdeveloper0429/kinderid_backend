const mongoose = require('mongoose');
const { Schema } = mongoose;


const childrenSchema = new Schema({
  childrenName: String,
  // wristband: {
  //   type: String, trim: true, index: {
  //     unique: true,
  //     partialFilterExpression: {wristband: {$type: 'string'}}
  //   }
  // },
  wristband: String,
  image: String,
  active: { type: Boolean, default: false },
  orderId: { type: Schema.Types.ObjectId, ref: 'Subscription'},
  subscription_start: { type: Date, default: Date.now() },
  subscription_end: Date,
  color: String,
  _parent: { type: Schema.Types.ObjectId, ref: 'Parent' }
});


module.exports = childrenSchema;
