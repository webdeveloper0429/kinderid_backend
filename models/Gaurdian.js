const mongoose = require('mongoose');
const { Schema } = mongoose;

const gaurdianSchema = new Schema({
  gaurdianName: String,
  gaurdianEmail: String,
  inviteStatus: { type: String, default: "Pending" },
  gaurdianRefId: String, // the parent id in Parent Model
  created_at: { type: Date, default: Date.now() },
  children: {type: Array, default: []},
  _gaurdianPushNotificationID: String,
  // _parent: { type: Schema.Types.ObjectId, ref: 'Parent' }
});


module.exports = gaurdianSchema;