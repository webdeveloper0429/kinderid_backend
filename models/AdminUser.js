const mongoose = require('mongoose');
const shortid = require('shortid');
const { Schema } = mongoose;

const adminUserSchema = new Schema({
  googleId: { type: String, unique: true },
  fullName: String,
  role: { type: String, default: "Admin" },
  email: String,
  publicId: { type: String, unique: true, 'default': shortid.generate },
  mobile: String,
  imageURL: String,
  active: { type: Boolean, default: true },
  created_at: Date
});

mongoose.model('adminusers', adminUserSchema);
