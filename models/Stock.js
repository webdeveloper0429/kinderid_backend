const mongoose = require('mongoose');
const { Schema } = mongoose;

const stockSchema = new Schema({
    prefix: String,
    serialNo: String,
    created_at: {type: Date, default: Date.now},
    color: String
});

mongoose.model('stock', stockSchema);
