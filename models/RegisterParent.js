const mongoose = require('mongoose');
const { Schema } = mongoose;

const registerParentSchema = new Schema({
  email: String,
  parentName: String,
  parentTel: String,
  parentPassword: String,
  address: String,
  post: String,
  city: String,
  verificationCode: String,
  createdAt: { type: Date, expires: 120, default: Date.now },
  invitingParentIds: Array,
});

// registerParentSchema.index({ created_at: 1 }, { expireAfterSeconds: 60*59*24 });
// TODO expires is not working, check it later

mongoose.model('registerparents', registerParentSchema);