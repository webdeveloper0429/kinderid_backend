const mongoose = require('mongoose');

const { Schema } = mongoose;
const childrenSchema = require('./Children');
const gaurdianSchema = require('./Gaurdian');
const bcrypt = require('bcrypt');

const SALT_WORK_FACTOR = 10;

const parentSchema = new Schema({
  email: {type: String, unique: true, required: true },
  parentName: String,
  mobile: String,
  status: {
    type: String,
    enum: ['USER', 'NOCHILD', 'GAURDIAN', 'INVITED', 'NEW'],
    default: 'NEW'
  },
  created_at: { type: Date, default: Date.now() },
  children: [childrenSchema],
  gaurdians: [gaurdianSchema],
  address: String,
  postalCode: String,
  city: String,
  country: String,
  zipCode: String,
  password: String,
  pushNotificationID: String,
  active: { type: Boolean, default: true },
  login_attemps: {type: Number, default: 0},
  stripeCustomerId: String,
  currency: String,
  invitingParentIds: Array,
  verificationCode: String,
  card: Object,
});

parentSchema.pre('save', function (next) {
  const user = this;
  if (!user.isModified('password')) return next();

  bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
      if (err) return next(err);
      bcrypt.hash(user.password, salt, (err, hash) => {
          if (err) return next(err);
          user.password = hash;
          next();
      });
  });
});

parentSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
      if (err) return cb(err);
      cb(null, isMatch);
  });
};

mongoose.model('Parent', parentSchema);
