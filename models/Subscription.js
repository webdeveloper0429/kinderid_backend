const mongoose = require('mongoose');
const { Schema } = mongoose;

const CounterSchema = new Schema({
    _id: {type: String, required: true},
    seq: { type: Number, default: 0 }
});
const counter = mongoose.model('counter', CounterSchema);

const subscriptionSchema = new Schema({
  orderNo: String,
  parentsID: String,
  parentName: String,
  childID: String,
  childName: String,
  subscription_start: { type: Date, default: Date.now() },
  subscription_end: Date,
  wristband: String,
  stripeResponse: Object,
  //wristbandColor: String,
});

subscriptionSchema.pre('save', function(next) {
    const doc = this;
        counter.findByIdAndUpdate({_id: 'entityId'}, {$inc: { seq: 1} }, {new: true, upsert: true}).then(function(count) {
            console.log("...count: "+JSON.stringify(count));
            doc.orderNo = count.seq;
            next();
        })
        .catch(function(error) {
            throw error;
        });
});

mongoose.model('Subscription', subscriptionSchema); 