const mongoose = require('mongoose');
const { Schema } = mongoose;

const subscriberSchema = new Schema({
    email: String,
    created_at: Date
});

mongoose.model('subscribers', subscriberSchema);
